package Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Pages.SignUpPage3;
import Pages.BigPage4;
import Pages.FakeLandingPage5;
import Pages.FakePricingPage6;
import Pages.FillOutForms7;
import Pages.InteractionElementPage9;
import Pages.LearntoAutomatePage8;
import Pages.LoginAutomation2;
import Pages.UltimateqaHomepage1;

public class AutomationExercisePage {
	public static WebDriver driver = null;

	public static void main(String[] args) throws InterruptedException {
		// Step1: InvokeBrowser
		invokeBrowser();
		// Step2: Perform actions on
		SignUpFlow();
		//Step3: Close Browser
				closeBrowser();
	}

	public static void invokeBrowser() {
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		driver = new ChromeDriver();

		driver.navigate().to("https://ultimateqa.com/automation/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		if (driver.getTitle().contains("Ultimate QA")) {
			System.out.println(driver.getTitle() + "-------------------was launched");
		} else {
			System.out.println("Fail Browser was not invoked----------------------");

		}
	}

	public static void SignUpFlow() throws InterruptedException {

		
		  UltimateqaHomepage1 obj = new UltimateqaHomepage1(driver);
		  obj.clickAutomationExercise();
		  
		  LoginAutomation2 obj2 = new LoginAutomation2(driver); 
		  obj2.clickLogin();
		  
		  SignUpPage3 obj3 = new SignUpPage3(driver); 
		  obj3.clickCreateanAccount();
		  obj3.enterFirstName("Pragya"); 
		  obj3.enterLastName("Lama");
		  obj3.enterEmail("plama25@gmail.com"); 
		  obj3.enterPassword("Abcdef112");
		  obj3.clickCheckBox();
		  
		  BigPage4 obj4 = new BigPage4(driver); 
		  obj4.clickBigPage();
		  obj4.clickFacebook(); 
		  obj4.enterName("Pragya");
		  obj4.enterEmail("pragya_lama21@yahoo.com");
		  obj4.enterMessage("Hi! This is Pragya"); 
		  String question =driver.findElement(By .xpath("//div[@id='et_pb_contact_form_0']//div[@class='et_contact_bottom_container']//span")).getText().trim(); 
		  String remove = question.replaceAll("\\s+", ""); 
		  String[] add = remove.split("\\+"); 
		  String firstpart = add[0]; 
		  String secondpart = add[1]; 
		  String[] parts11 = secondpart.split("\\="); 
		  String lastpart1 = parts11[0]; 
		  int summ = Integer.parseInt(firstpart)+ Integer.parseInt(lastpart1); 
		  String a = String.valueOf(summ); 
		  obj4.enterValue(a); 
		  obj4.clickSubmit();
		  
		  FakeLandingPage5 obj5 = new FakeLandingPage5(driver);
		  obj5.clickFakeLandingPage5();  
		  
		  
		  FakePricingPage6 obj6 = new FakePricingPage6(driver);
		  obj6.clickFakePricing();
		  obj6.clickBasicPurchase();
		  
		  FillOutForms7 obj7 = new FillOutForms7(driver); 
		  obj7.clickFillOutFormms();
		  obj7.enterName("Java");
		  obj7.enterMessage("Java is a general-purpose programming language");
		  obj7.clickSubmit(); 
		  obj7.enterName1("Java");
		  obj7.enterMessage1("Java is a general-purpose programming language"); 
		  String capchavalue = driver.findElement(By.xpath("//span[@class ='et_pb_contact_captcha_question']")).getText() .trim();
		  String removespace = capchavalue.replaceAll("\\s+", ""); 
		  String[] parts = removespace.split("\\+"); 
		  String part1 = parts[0]; 
		  String part2 = parts[1];
		  String[] parts1 = part2.split("\\="); 
		  String Lastpart = parts1[0]; 
		  int summation = Integer.parseInt(part1) + Integer.parseInt(Lastpart); 
		  String s = String.valueOf(summation); 
		  obj7.enterValue(s);
		  System.out.println("The Values are :" + s); 
		  obj7.clickSubmit1();
		 

		LearntoAutomatePage8 obj8 = new LearntoAutomatePage8(driver);
		obj8.clickLearntoAutomate();
		obj8.enterName("Cerotid");
		obj8.clickSubmit();
		obj8.enterName("Test");
		obj8.clickSubmit();
		obj8.clickSprint2();
		obj8.enterFirstName("Test2");
		obj8.enterLastName("test");
		obj8.clickSubmit1();
		obj8.clickSprint3();
		obj8.clickFemale();
		obj8.enterFirstName1("Tester");
		obj8.enterLastName1("Solution");
		obj8.clickFacebook();

		InteractionElementPage9 obj9 = new InteractionElementPage9(driver);
		obj9.clickInteractionPage();
		obj9.clickButtonusingID();

	}
	public static void closeBrowser() {
		driver.close();
		driver.quit();
	}

}
