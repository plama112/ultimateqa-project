package TestNg;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import Configuration.PropertiesFile;
import Pages.BigPage4;
import Pages.FakeLandingPage5;
import Pages.FakePricingPage6;
import Pages.FillOutForms7;
import Pages.InteractionElementPage9;
import Pages.LearntoAutomatePage8;
import Pages.LoginAutomation2;
import Pages.SignUpPage3;
import Pages.UltimateqaHomepage1;

public class UltimateQATest {

	ExtentHtmlReporter htmlReporter;
	static ExtentReports extent;
	ExtentTest test;
	
	static WebDriver driver;
	public  static String browserName = null;

	@BeforeSuite
	
	public void setUp() {
		htmlReporter = new ExtentHtmlReporter("UltimateQA.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
	}
	
	@BeforeTest
	public void setUpTest() {
	
		PropertiesFile.getProperties();
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
			driver = new ChromeDriver();
		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./libs/geckodriver");
			driver = new FirefoxDriver();
		}
		driver.navigate().to("https://ultimateqa.com");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		if (driver.getTitle().contains("Ultimate QA")) {
			System.out.println(driver.getTitle() + "-------------------was launched");
		} else {
			System.out.println("Fail Browser was not invoked----------------------");

		}
		ExtentTest isBrowserOpened = extent.createTest("Invoke Browser",
				"This websites ensures that the browser is invoked");
		isBrowserOpened.pass("Browser was invoked as Expected");

		
	}

	@Test(priority = 0)
	public  void UltimateHomepage() {
		
		ExtentTest test= extent.createTest("UltimateQA", "This Page ensures it is launched");
		test.log(Status.INFO, "Homepage Launched");
		
		ExtentTest test1 = extent.createTest("Pragya Lama","This page is launched");
		test1.log(Status.INFO, "Homepage launched");


		UltimateqaHomepage1 obj = new UltimateqaHomepage1(driver);
		obj.clickAutomationExercise();
	}

	@Test(priority = 1)
	public void LoginAutomation() {
		ExtentTest test2 = extent.createTest("UltimateQA Login Page", "This Page ensures the login page is opened");
		test2.log(Status.INFO, "Login Page");

		LoginAutomation2 obj2 = new LoginAutomation2(driver);
		obj2.clickLogin();
	}

	@Test(priority = 2)
	public  void SignUpPage() throws InterruptedException {
		ExtentTest test3 = extent.createTest("Ultimate SignUp Page","This page allows us to sign up ");
	    test3.log(Status.INFO, "SignUpPage");
		SignUpPage3 obj3 = new SignUpPage3(driver);
		obj3.clickCreateanAccount();
		obj3.enterFirstName("Pragya");
		obj3.enterLastName("Lama");
		obj3.enterEmail("plama25@gmail.com");
		obj3.enterPassword("Abcdef112");
		obj3.clickCheckBox();
	}

	@Test(priority = 3)
	public void BigPage() throws InterruptedException {
		ExtentTest test4 = extent.createTest("Ultimate BigPage","This page allows us to browse website");
	    test4.log(Status.INFO, "Ultimate BigPage Launched");
		BigPage4 obj4 = new BigPage4(driver);
		obj4.clickBigPage();
		  obj4.clickFacebook(); 
		  obj4.enterName("Pragya");
		  obj4.enterEmail("pragya_lama21@yahoo.com");
		  obj4.enterMessage("Hi! This is Pragya"); 
		  String question =driver.findElement(By .xpath("//div[@id='et_pb_contact_form_0']//div[@class='et_contact_bottom_container']//span")).getText().trim(); 
		  String remove = question.replaceAll("\\s+", ""); 
		  String[] add = remove.split("\\+"); 
		  String firstpart = add[0]; 
		  String secondpart = add[1]; 
		  String[] parts11 = secondpart.split("\\="); 
		  String lastpart1 = parts11[0]; 
		  int summ = Integer.parseInt(firstpart)+ Integer.parseInt(lastpart1); 
		  String a = String.valueOf(summ); 
		  obj4.enterValue(a); 
		  obj4.clickSubmit();
	}

	@Test(priority = 4)
	public void FakeLandingPage() throws InterruptedException {
		ExtentTest test5 = extent.createTest("FakeLandingPage","This page allows us to view FakelandingPrice");
	    test5.log(Status.INFO, "FakeLandingPage Launched");
		FakeLandingPage5 obj5 = new FakeLandingPage5(driver);
		obj5.clickFakeLandingPage5();
	
	}

	@Test(priority = 5)
	public void FakePricingPage() throws InterruptedException {
		ExtentTest test6 = extent.createTest("FakePricingPage","This page allows us to purchase course");
        test6.log(Status.INFO, "FakePricingPage launched");
		FakePricingPage6 obj6 = new FakePricingPage6(driver);
		obj6.clickFakePricing();
		obj6.clickBasicPurchase();
	}

	@Test(priority = 6)
	public void FillOutForms() throws InterruptedException {
		ExtentTest test7 = extent.createTest("FillOutForms","This page allows us to fill names, message and submit it");
        test7.log(Status.INFO, "FillOutFormsPage launched");
		FillOutForms7 obj7 = new FillOutForms7(driver);
		obj7.clickFillOutFormms();
		 obj7.enterName("Java");
		  obj7.enterMessage("Java is a general-purpose programming language");
		  obj7.clickSubmit(); obj7.enterName1("Java");
		  obj7.enterMessage1("Java is a general-purpose programming language"); 
		  String capchavalue = driver.findElement(By.xpath("//span[@class ='et_pb_contact_captcha_question']")).getText() .trim();
		  String removespace = capchavalue.replaceAll("\\s+", ""); 
		  String[] parts = removespace.split("\\+"); 
		  String part1 = parts[0]; 
		  String part2 = parts[1];
		  String[] parts1 = part2.split("\\="); 
		  String Lastpart = parts1[0]; 
		  int summation = Integer.parseInt(part1) + Integer.parseInt(Lastpart); 
		  String s = String.valueOf(summation); 
		  obj7.enterValue(s);
		  System.out.println("The Values are :" + s); 
		  obj7.clickSubmit1();
	}

	@Test(priority = 7)
	public void LearntoAutomate() throws InterruptedException {
		ExtentTest test8 = extent.createTest("LearntoAutomate","This page allows us to write name, submit and use sprint1 and 2");
        test8.log(Status.INFO, "LearntoAutomate Page launched");
		LearntoAutomatePage8 obj8 = new LearntoAutomatePage8(driver);
		obj8.clickLearntoAutomate();
		obj8.enterName("Cerotid");
		obj8.clickSubmit();
		obj8.enterName("Test");
		obj8.clickSubmit();
		obj8.clickSprint2();
		obj8.enterFirstName("Test2");
		obj8.enterLastName("test");
		obj8.clickSubmit1();
		obj8.clickSprint3();
		obj8.clickFemale();
		obj8.enterFirstName1("Tester");
		obj8.enterLastName1("Solution");
		obj8.clickFacebook();
	}

	@Test(priority = 8)
	public void Interaction() throws InterruptedException {
		ExtentTest test9 = extent.createTest("Interaction Element","This page allows us to hover the mouse button");
        test9.log(Status.INFO, "Interaction Element Page launched");
		InteractionElementPage9 obj9 = new InteractionElementPage9(driver);
		obj9.clickInteractionPage();
		obj9.clickButtonusingID();

	}
	
	@AfterTest

	public void terminateTest() {
		ExtentTest test10 = extent.createTest("UltimateQA Homepage","This page is closed");
        test10.log(Status.INFO, "Terminate the Browser");
		driver.close();
		//driver.quit();
		System.out.println("Terminating the Browser");

	}

	@AfterSuite
	public void tearDown() {
		extent.flush();

	}

}
