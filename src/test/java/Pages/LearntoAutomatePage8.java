package Pages;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LearntoAutomatePage8 {
	WebDriver driver = null;
	WebElement element = null;
	
	By clickLearntoAutomate = By .xpath("//div[@class='et_pb_text_inner']/ul/li[5]/a");
	By enterName = By .xpath("//input[@name='firstname']");
	By clickSubmit = By .xpath("//input[@id='submitForm']");
	By clickSprint2 = By .xpath("//a[contains(text(),'Go to the next sprint')] ");
	By enterFirstName = By .xpath("//input[@name='firstname']");
	By enterLastName = By .xpath("//input[@name='lastname']");
	By clickSubmit1 = By .xpath("//div[@id='left-area']//input[3]");
	By clickSprint3 = By .xpath("//a[contains(text(),'Go to sprint 3')]");
	By clickMale = By .xpath("//input[@value='male']");
	By clickFemale = By .xpath("//input[@value='female']");
	By enterFirstName1 = By .xpath("//input[@name='firstname']"); 
	By enterLastName1 = By .xpath("//input[@name='lastname']");
	By clickFacebook = By .xpath("//div[@class='swp_social_panel swp_horizontal_panel swp_flat_fresh swp_default_full_color swp_other_full_color swp_individual_full_color scale-100 scale-full_width']//div[@class='nc_tweetContainer swp_share_button swp_facebook']//span[@class='iconFiller']");
    By clickTwitter = By .xpath("//div[@class='swp_social_panel swp_horizontal_panel swp_flat_fresh swp_default_full_color swp_other_full_color swp_individual_full_color scale-100 scale-full_width']//div[@class='nc_tweetContainer swp_share_button swp_twitter']//span[@class='iconFiller']");
	
	
	
	public LearntoAutomatePage8(WebDriver driver) {
		this.driver = driver;
	}
	public void clickLearntoAutomate() throws InterruptedException {
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(clickLearntoAutomate).click();
	}
	public void enterName(String Name) {
		driver.findElement(enterName).sendKeys(Name);
		
	}
	public void clickSubmit() throws InterruptedException {
		driver.findElement(clickSubmit).click();
		TimeUnit.SECONDS.sleep(5);
		driver.navigate().to("https://ultimateqa.com/sample-application-lifecycle-sprint-1/");
	}
	public void clickSprint2() throws InterruptedException {
		driver.findElement(clickSprint2).click();
		TimeUnit.SECONDS.sleep(5);
	}
	
	public void enterFirstName(String First) {
		driver.findElement(enterFirstName).sendKeys(First);
	}
	public void enterLastName(String Last) {
		driver.findElement(enterLastName).sendKeys(Last);
	}
	public void clickSubmit1() throws InterruptedException {
		driver.findElement(clickSubmit1).click();
		TimeUnit.SECONDS.sleep(5);
		driver.navigate().to("https://ultimateqa.com/sample-application-lifecycle-sprint-2/");
	}
	public void clickSprint3() {
		driver.findElement(clickSprint3).click();
		
	}
	public void clickMale() {
		driver.findElement(clickMale).click();
	}
	public void clickFemale() throws InterruptedException {
		driver.findElement(clickFemale).click();
		TimeUnit.SECONDS.sleep(5);
	}
	public void enterFirstName1(String First1) {
		driver.findElement(enterFirstName1).sendKeys(First1);
	}
	public void enterLastName1(String Last1) throws InterruptedException {
		driver.findElement(enterLastName1).sendKeys(Last1);
		TimeUnit.SECONDS.sleep(5);
	}
	

	public void clickFacebook() throws InterruptedException {
		driver.findElement(clickFacebook).click();
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(clickTwitter).click();
		TimeUnit.SECONDS.sleep(5);
		String mainWindow = driver.getWindowHandle();
		Set<String>set = driver.getWindowHandles();
		Iterator<String> itr = set.iterator();
		for(String child: set) 
		{
			String childWindow = itr.next();
			if(!mainWindow.equals(childWindow))
			{
				driver.switchTo().window(childWindow) ;
				System.out.println(driver.switchTo().window(childWindow).getTitle());
				TimeUnit.SECONDS.sleep(5);
				System.out.println("Facebook Page was Opened");
				driver.close();
				
			}
			TimeUnit.SECONDS.sleep(5);
			driver.switchTo().window(mainWindow);
			//driver.close();
		
			break;
		}
		Screenshot.captureScreenShots(driver, "LearntoAutomatePage");
		System.out.println("Twitter Page was Opened");
		driver.navigate().to("https://ultimateqa.com/automation/");
	}
	
	}


