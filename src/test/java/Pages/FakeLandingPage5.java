package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FakeLandingPage5 {
	WebDriver driver = null;
	WebElement element = null;
   
	
	By clickFakeLandingPage = By .xpath("//div[@class='et_pb_text_inner']/ul/li[2]/a");
	
	
	public FakeLandingPage5(WebDriver driver) {
		this.driver = driver;
		
	}
	
	public void clickFakeLandingPage5() throws InterruptedException {
	    Thread.sleep(1100);
        driver.findElement(clickFakeLandingPage).click();	
        JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1500)","");
		TimeUnit.SECONDS.sleep(5);
		js.executeScript("window.scrollBy(0,-1500)","" );
		Screenshot.captureScreenShots(driver, "FakeLandingPrice");
	    Thread.sleep(1000);
	    driver.navigate().to("https://ultimateqa.com/automation/");
	    
	}

	
}
