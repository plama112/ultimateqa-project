package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class FakePricingPage6 {
	WebDriver driver = null;
	WebElement element = null;
	
	By clickFakePricing = By .xpath("//div[@class='et_pb_text_inner']/ul/li[3]/a");
	By clickBasicPurchase = By .xpath("(//a[@class='et_pb_button et_pb_pricing_table_button'])[2]");
	
	public FakePricingPage6(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickFakePricing() throws InterruptedException {
		driver.findElement(clickFakePricing).click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,400)","");
		TimeUnit.SECONDS.sleep(5);
		js.executeScript("window.scrollBy(0,-400)","" );
		Thread.sleep(1000);
		
	}
	
	public void clickBasicPurchase() throws InterruptedException {
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElement(clickBasicPurchase)).build().perform();
		driver.findElement(clickBasicPurchase).click();
		Screenshot.captureScreenShots(driver, "FakePricingPage");
		Thread.sleep(1100);
		driver.navigate().to("https://ultimateqa.com/automation/");
	}
}
