package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginAutomation2 {
	WebDriver driver = null;
	WebElement element = null;
   
	By clickLogin = By .xpath("//div[@class='et_pb_text_inner']/ul/li[6]/a");
	 
	public LoginAutomation2(WebDriver driver) {
		this.driver = driver;
	
		
	}
	public void clickLogin() {
		Screenshot.captureScreenShots(driver, "LoginPage");
		driver.findElement(clickLogin).click();
	}
	
}
