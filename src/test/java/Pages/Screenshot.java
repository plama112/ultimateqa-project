package Pages;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Screenshot {
	public static void captureScreenShots(WebDriver drivers, String screenshot)
	{
		try 
		{
			File src = ((TakesScreenshot)drivers).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src, new File("/Users/Nick/Documents/spring/SeleniumFinalProject/ScreenShot Files/" + screenshot + ".png"));
			System.out.println("Screenshot Taken");

		} catch (Exception e) 
		{

			System.out.println("Exception while taking screenshot" + e.getMessage());
		}
	}
}

