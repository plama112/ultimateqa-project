package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BigPage4 {
	WebDriver driver = null;
	WebElement element = null;
   
	

	
	By clickBigPage = By .xpath("//div[@class='et_pb_text_inner']/ul/li[1]/a"); 
	By clickFacebook = By .xpath("//li[@class='et_pb_social_media_follow_network_1 et_pb_social_icon et_pb_social_network_link et-social-facebook et_pb_social_media_follow_network_1']//a[@class='icon et_pb_with_border']");
    By enterName = By .xpath("//input[@id='et_pb_contact_name_0']");
	By enterEmail = By .xpath("//input[@id='et_pb_contact_email_0']");
	By enterMessage = By .xpath("//textarea[@id='et_pb_contact_message_0']");
	By enterNumber= By .xpath("//div[@id='et_pb_contact_form_0']//div[@class='et_contact_bottom_container']//span");
	By enterValue = By .xpath("//input[@name='et_pb_contact_captcha_0']");
	By clickSubmit = By .xpath("//div[@id='et_pb_contact_form_0']//button[@name='et_builder_submit_button'][contains(text(),'Submit')]");	 
	
	
	public BigPage4(WebDriver driver) {
		this.driver = driver;
		
	}
	
	public void clickBigPage() throws InterruptedException {
		driver.findElement(clickBigPage).click();
		Thread.sleep(1100);
	}
	
	public void clickFacebook() throws InterruptedException {
		driver.findElement(clickFacebook).click();
		TimeUnit.SECONDS.sleep(3);
		driver.navigate().to("https://ultimateqa.com/complicated-page");
		TimeUnit.SECONDS.sleep(3);
	}
	
	
	public void enterName(String Name) {
		driver.findElement(enterName).sendKeys(Name);
	}
	public void enterEmail(String Email) {
		driver.findElement(enterEmail).sendKeys(Email);
	}
	public void enterMessage(String Message) {
		driver.findElement(enterMessage).sendKeys(Message);
		}
	public void enterNumber(String Number) {
		driver.findElement(enterNumber).sendKeys(Number);
	}
	public void enterValue(String Value) {
		driver.findElement(enterValue).sendKeys(Value);
	}
	public void clickSubmit() throws InterruptedException {
		Screenshot.captureScreenShots(driver, "BigPage");
		driver.findElement(clickSubmit).click();
		Thread.sleep(1100);
		driver.navigate().to("https://ultimateqa.com/automation/");
		
	}
}
