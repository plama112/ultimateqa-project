package Pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SignUpPage3 {
	WebDriver driver = null;
	WebElement element = null;
	
	By clickCreateanAccount = By .xpath("//aside[@class='sign-in__sign-up']/a");
	By enterFirstName = By .xpath("//input[@id='user[first_name]']");
	By enterLastName = By .xpath("//input[@id='user[last_name]']");
	By enterEmail = By .xpath("//input[@id='user[email]']");
	By enterPassword = By .xpath("//input[@id='user[password]']");
	By clickCheckbox = By .xpath("//input[@id='user[terms]']");
	
	
	public SignUpPage3(WebDriver driver) {
		this.driver = driver;
	
}
	public void clickCreateanAccount() {
		driver.findElement(clickCreateanAccount).click();
	}
	public void enterFirstName(String First) {
		driver.findElement(enterFirstName).sendKeys(First);
	}
	public void enterLastName(String Last) {
		driver.findElement(enterLastName).sendKeys(Last);
	}
	public void enterEmail(String Email) {
		driver.findElement(enterEmail).sendKeys(Email);
	}
	
	public void enterPassword(String Password) {
		driver.findElement(enterPassword).sendKeys(Password);
	}
	public void clickCheckBox() throws InterruptedException {
		driver.findElement(clickCheckbox).click();
		Screenshot.captureScreenShots(driver, "SignUpPage");
		Thread.sleep(1100);
		driver.navigate().to("https://ultimateqa.com/automation/");
	}
	
}
