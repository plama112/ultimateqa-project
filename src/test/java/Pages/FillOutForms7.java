package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FillOutForms7 {
	WebDriver driver = null;
	WebElement element = null;
	
	By clickFillOutForms = By .xpath("//div[@class='et_pb_text_inner']/ul/li[4]/a");
    By enterName = By .xpath("//input[@id='et_pb_contact_name_0']");
    By enterMessage = By .xpath("//textarea[@id='et_pb_contact_message_0']");
    By clickSubmit = By .xpath("//div[@id='et_pb_contact_form_0']//button[@name='et_builder_submit_button'][contains(text(),'Submit')]");
    By enterName1 = By .xpath("//input[@id='et_pb_contact_name_1']");
    By enterMessage1 = By .xpath("//textarea[@id='et_pb_contact_message_1']");
    By enterNumber = By .xpath("//span[@class ='et_pb_contact_captcha_question']");
    By enterValue = By .xpath("//input[@class ='input et_pb_contact_captcha']");
    By clickSubmit1 = By .xpath("//div[@id='et_pb_contact_form_1']//button[@name='et_builder_submit_button'][contains(text(),'Submit')]");

    public FillOutForms7(WebDriver driver) {
    	this.driver = driver;
    	
    }

    public void clickFillOutFormms() {
    	driver.findElement(clickFillOutForms).click();
    }
    public void enterName(String Name) {
    	driver.findElement(enterName).sendKeys(Name);
    }
    public void enterMessage(String Message) {
    	driver.findElement(enterMessage).sendKeys(Message);
    }
    public void clickSubmit() throws InterruptedException {
    	driver.findElement(clickSubmit).click();
    	TimeUnit.SECONDS.sleep(5);
//    	driver.findElement(clickSubmit).click();
//    	TimeUnit.SECONDS.sleep(5);
//    	driver.findElement(clickSubmit).click();
//    	TimeUnit.SECONDS.sleep(5);
    }
    public void enterName1(String Name1) {
    	driver.findElement(enterName1).sendKeys(Name1);
    }
    public void enterMessage1(String Message1) {
    	driver.findElement(enterMessage1).sendKeys(Message1);
    }
    
        
    public void enterNumber(String Number) {
    	driver.findElement(enterNumber).sendKeys(Number);
    }
  
	 public void enterValue(String Value) throws InterruptedException {
		 TimeUnit.SECONDS.sleep(4);
		 driver.findElement(enterValue).sendKeys(Value);
    }
    
    public void clickSubmit1() throws InterruptedException {
    	driver.findElement(clickSubmit1).click();
    	Screenshot.captureScreenShots(driver, "FillOutForms");
    	TimeUnit.SECONDS.sleep(3);
    	driver.navigate().to("https://ultimateqa.com/automation/");
    }
}

