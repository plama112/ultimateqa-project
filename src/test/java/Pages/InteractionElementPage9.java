package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class InteractionElementPage9 {
	WebDriver driver = null;
	WebElement element = null;
	
	
	By clickInteractionPage = By .xpath("//div[@class='et_pb_text_inner']/ul/li[7]/a");
	By clickButtonusingID = By .xpath("(//a[@id='idExample'])");
	
	
	public InteractionElementPage9(WebDriver driver) {
		this.driver = driver;
		
	}

	public void clickInteractionPage() {
		driver.findElement(clickInteractionPage).click();
	}
	public void clickButtonusingID() throws InterruptedException {
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElement(clickButtonusingID)).build().perform();
		driver.findElement(clickButtonusingID).click();
		
		Screenshot.captureScreenShots(driver, "InteractionElementPage");
		TimeUnit.SECONDS.sleep(5);
		driver.navigate().to("https://ultimateqa.com/automation/");

	}
}
