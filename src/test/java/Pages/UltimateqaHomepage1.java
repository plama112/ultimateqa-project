package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UltimateqaHomepage1 {
	WebDriver driver = null;
	WebElement element = null;

	By clickAutomationExercise = By .xpath("//li[@id ='menu-item-587']");
	
	public UltimateqaHomepage1(WebDriver driver) {
		this.driver = driver;
		
	}
	public void clickAutomationExercise() {
		Screenshot.captureScreenShots(driver, "UltimateqaHomepage");
		driver.findElement(clickAutomationExercise).click();
		
	}
 
}
