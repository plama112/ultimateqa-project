package Configuration;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import TestNg.UltimateQATest;

public class PropertiesFile {
	static Properties prop = new Properties();
	static String projectPath = System.getProperty("user.dir");
	
	public static void main(String[] args) {
		getProperties();
		setProperties();
		getProperties();
		
	}
	public static void getProperties() {
		
		try {
			InputStream input = new FileInputStream(projectPath + "/src/test/java/Configuration/config.properties");
			prop.load(input);
			String browser = prop.getProperty("browser");
			System.out.println(browser +" Was Invoked");
			UltimateQATest.browserName = browser;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}
		
		}
	public static void setProperties() {
		
		try {
			OutputStream output = new FileOutputStream(projectPath + "/src/test/java/Configuration/config.properties");
			prop.setProperty("Result", "Pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}
		
	}

}
